# Front

> Front project with Html, CSS

## Prerequisites
Make sure you have installed all of the following prerequisites on your development machine:

* Git - [Download & Install Git](https://git-scm.com/downloads).

## Cloning The Bitbucket Repository

```bash
git clone https://digoribeiro@bitbucket.org/digoribeiro/be-interactive-front.git
```
